<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/**
 *
 * Mijnlieff main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *       
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall( "/mijnlieff/mijnlieff/myAction.html", ...)
 *
 */

class action_mijnlieff extends APP_GameAction {
    // Constructor: please do not modify
    public function __default() {
        if ( self::isArg('notifwindow') ) {
            $this->view = 'common_notifwindow';
            $this->viewArgs['table'] = self::getArg('table', AT_posint, true);
        }
        else {
            $this->view = 'mijnlieff_mijnlieff';
            self::trace('Complete reinitialization of board game');
        }
    }
  	
    public function place_board () {
        self::setAjaxMode();
        $this->game->place_board(
            self::getArg('x', AT_int, true),
            self::getArg('y', AT_int, true)
        );
        self::ajaxResponse();
    }

    public function place_piece () {
        self::setAjaxMode();
        $this->game->place_piece(
            self::getArg('x', AT_int, true),
            self::getArg('y', AT_int, true),
            self::getArg('piece_id', AT_posint, true)
        );
        self::ajaxResponse();
    }
}
