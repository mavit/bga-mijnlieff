# BGA Mijnlieff

[Mijnlieff by Andy Hopwood](https://www.hopwoodgames.com/shop-1/mijnlieff) is a simple abstract strategy game for two players that plays in about five minutes.  Here, it is implemented for [Board Game Arena](https://boardgamearena.com/gamepanel?game=mijnlieff&sp=1e718n).

## Discussion

You can discuss this version of the game in Board Game Arenaʼs Mijnlieff [forum](https://boardgamearena.com/forum/viewforum.php?f=332&sp=1e718n) or [players group](https://boardgamearena.com/group?id=7188272&sp=1e718n).

## Reporting bugs

You are welcome to report bugs either [on GitLab](https://gitlab.com/mavit/bga-mijnlieff/-/issues) or at [BGA Bugs & Suggestions](https://boardgamearena.com/bugs?game=1283&sp=1e718n).

## Translations

See the [translation page on Board Game Arena](https://boardgamearena.com/translation?source_locale=en_US&dest_locale=en_US&module_id=1283&sp=1e718n) to provide or update translations for your language.

## Software Licence

BGA Mijnlieff is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

BGA Mijnlieff is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

This code has been produced on the BGA Studio platform for use on https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio for more information.

# Artwork

The artwork is not a part of BGA Mijnlieff, and is copyright [XV Games](http://www.xvgames.it/).
