<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/*
   Game state machine is a tool used to facilitate game developpement by doing common stuff that can be set up
   in a very easy way from this configuration file.

   Please check the BGA Studio presentation about game state to understand this, and associated documentation.

   Summary:

   States types:
   _ activeplayer: in this type of state, we expect some action from the active player.
   _ multipleactiveplayer: in this type of state, we expect some action from multiple players (the active players)
   _ game: this is an intermediary state where we don't expect any actions from players. Your game logic must decide what is the next game state.
   _ manager: special type for initial and final state

   Arguments of game states:
   _ name: the name of the GameState, in order you can recognize it on your own code.
   _ description: the description of the current game state is always displayed in the action status bar on
                  the top of the game. Most of the time this is useless for game state with "game" type.
   _ descriptionmyturn: the description of the current game state when it's your turn.
   _ type: defines the type of game states (activeplayer / multipleactiveplayer / game / manager)
   _ action: name of the method to call when this game state become the current game state. Usually, the
             action method is prefixed by "st" (ex: "stMyGameStateName").
   _ possibleactions: array that specify possible player actions on this step. It allows you to use "checkAction"
                      method on both client side (Javacript: this.checkAction) and server side (PHP: self::checkAction).
   _ transitions: the transitions are the possible paths to go from a game state to another. You must name
                  transitions in order to use transition names in "nextState" PHP method, and use IDs to
                  specify the next game state for each transition.
   _ args: name of the method to call to retrieve arguments for this gamestate. Arguments are sent to the
           client side to be used on "onEnteringState" or to set arguments in the gamestate description.
   _ updateGameProgression: when specified, the game progression is updated (=> call to your getGameProgression
                            method).
*/

//    !! It is not a good idea to modify this file when a game is running !!

 
$machinestates = [

    // The initial state. Please do not modify.
    1 => [
        'name' => 'gameSetup',
        'description' => '',
        'type' => 'manager',
        'action' => 'stGameSetup',
        'transitions' => ['' => 10]
    ],

    10 => [
        'name' => 'check_layout',
        'description' => '',
        'type' => 'game',
        'action' => 'check_layout_action',
        'transitions' => [
            'standard' => 2,
            'players' => 11,
        ],
    ],

    11 => [
        'name' => 'check_start',
        'description' => '',
        'type' => 'game',
        'action' => 'check_start_action',
        'transitions' => [
            'started' => 2,
            'not_started' => 12
        ],
    ],
    12 => [
        'name' => 'player_setup_turn',
        'description' => clienttranslate('${actplayer} must lay a board segment'),
        'descriptionmyturn' => clienttranslate('${you} must lay a board segment'),
        'type' => 'activeplayer',
        'possibleactions' => ['place_board'],
        'transitions' => ['placed_board' => 13],
        'args' => 'player_setup_turn_arg',
    ],
    13 => [
        'name' => 'next_setup_turn',
        'description' => '',
        'type' => 'game',
        'action' => 'next_turn_action',
        'transitions' => ['' => 11],
    ],

    2 => [
        'name' => 'check_end',
        'description' => '',
        'type' => 'game',
        'action' => 'check_end_action',
        'transitions' => [
            'ended' => 99,
            'not_ended' => 3
        ],
    ],
    3 => [
        'name' => 'check_forced_pass',
        'description' => clienttranslate('${actplayer} is blocked!'),
        'descriptionmyturn' => clienttranslate('${you} are blocked!'),
        'type' => 'activeplayer',
        'action' => 'check_forced_pass_action',
        'transitions' => [
            'play' => 4,
            'pass' => 5,
        ],
    ],
    4 => [
        'name' => 'player_turn',
        'description' => clienttranslate('${actplayer} must place a piece'),
        'descriptionmyturn' => clienttranslate('${you} must place a piece'),
        'type' => 'activeplayer',
        'possibleactions' => ['place_piece'],
        'transitions' => [
            'placed_piece' => 5,
            'pass' => 5,
        ],
        'args' => 'player_turn_arg',
    ],
    5 => [
        'name' => 'next_turn',
        'description' => '',
        'type' => 'game',
        'action' => 'next_turn_action',
        'updateGameProgression' => true,
        'transitions' => ['' => 2],
    ],

    // Final state.
    // Please do not modify (and do not overload action/args methods).
    99 => [
        'name' => 'gameEnd',
        'description' => clienttranslate('End of game'),
        'type' => 'manager',
        'action' => 'stGameEnd',
        'args' => 'argGameEnd'
    ]
];
