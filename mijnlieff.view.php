<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/**
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in mijnlieff_mijnlieff.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
require_once( APP_BASE_PATH."view/common/game.view.php" );

class view_mijnlieff_mijnlieff extends game_view {
    function getGameName() {
        return "mijnlieff";
    }
    function build_page( $viewArgs ) {
        // Get players & players number
        $players = $this->game->loadPlayersBasicInfos();
        $players_nbr = count($players);

        $this->page->begin_block('mijnlieff_mijnlieff', 'standard_layout');
        $this->page->begin_block('mijnlieff_mijnlieff', 'scrollmap_layout');
        if ( $this->game->getGameStateValue('layout') == 1 ) {
            $this->page->insert_block('standard_layout', []);
        }
        else {
            $this->tpl['LABEL_ENLARGE_SCROLLMAP'] = self::_('Enlarge display');
            $this->tpl['LABEL_TOUCH_TIP'] = self::_('Tip: long press to confirm board position before tapping to lay');
            $this->page->insert_block('scrollmap_layout', []);
        }
        
        /*
        
        // Examples: set the value of some element defined in your tpl file like this: {MY_VARIABLE_ELEMENT}

        // Display a specific number / string
        $this->tpl['MY_VARIABLE_ELEMENT'] = $number_to_display;

        // Display a string to be translated in all languages: 
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::_("A string to be translated");

        // Display some HTML content of your own:
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::raw( $some_html_code );
        
        */
        
        /*
        
        // Example: display a specific HTML block for each player in this game.
        // (note: the block is defined in your .tpl file like this:
        //      <!-- BEGIN myblock --> 
        //          ... my HTML code ...
        //      <!-- END myblock --> 
        

        $this->page->begin_block( "mijnlieff_mijnlieff", "myblock" );
        foreach( $players as $player )
        {
            $this->page->insert_block( "myblock", array( 
                                                    "PLAYER_NAME" => $player['player_name'],
                                                    "SOME_VARIABLE" => $some_value
                                                    ...
                                                     ) );
        }
        
        */
    }
}
