<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.


require_once(APP_GAMEMODULE_PATH.'module/table/table.game.php');

class Mijnlieff extends Table {
    function __construct() {
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();

        self::initGameStateLabels([
            //    "my_first_global_variable" => 10,
            //    "my_second_global_variable" => 11,
            //      ...
            //    "my_first_game_variant" => 100,
            //    "my_second_game_variant" => 101,
            //      ...
            'layout' => 100,
            'rules' => 101,
        ]);
    }

    protected function getGameName () {
        // Used for translations and stuff. Please do not modify.
        return "mijnlieff";
    }

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game
        rules, so that the game is ready to be played.
    */
    protected function setupNewGame ($players, $options = []) {
        $gameinfos = self::getGameinfos();
        $default_colors = $gameinfos['player_colors'];
 
        // Create players
        foreach ( $players as $player_id => $player ) {
            $color = array_shift($default_colors);
            self::DbQuery(
                self::sql_placeholders(
                    'insert into `player`
                     set `player_id` = ?,
                         `player_color` = ?,
                         `player_canal` = ?,
                         `player_name` = ?,
                         `player_avatar` = ?',
                    $player_id,
                    $color,
                    $player['player_canal'],
                    $player['player_name'],
                    $player['player_avatar']
                )
            );
            self::initStat(
                'player',
                'is_white',
                $color == $this->colors['white'],
                $player_id
            );

        }
        self::reloadPlayersBasicInfos();

        // Init game statistics
        self::initStat('table', 'turns_count', 0);
        self::initStat('player', 'played_count', 0);
        self::initStat('player', 'passes', 0);
        self::initStat('player', 'forced_passes', 0);

        // Populate pieces table:
        foreach ( [true, false] as $is_white ) {
            for ( $i = 0; $i < count($this->shapes) / 2; $i++ ) {
                for ( $j = 0; $j < $this->board['PIECES_PER_SHAPE']; $j++ ) {
                    self::DbQuery(
                        self::sql_placeholders(
                            'insert into piece (`shape`, `is_white`)
                                         values (?, ?)',
                            $this->shapes[$i],
                            $is_white
                        )
                    );
                }
            }
        }

        // Insert empty squares into database:
        self::insert_board(0, 0);
        if ( $this->getGameStateValue('layout') == 1 ) {
            self::insert_board(0, 2);
            self::insert_board(2, 0);
            self::insert_board(2, 2);
        }

        // White starts.
        $this->gamestate->changeActivePlayer(
            self::getNonEmptyObjectFromDb(
                self::sql_placeholders(
                    'select `player_id` from `player` where `player_color` = ?',
                    ( $this->getGameStateValue('layout') == 1
                      ? $this->colors['white']
                      : $this->colors['black'] )
                )
            )['player_id']
        );
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by
        the current player).
        
        The method is called each time the game interface is displayed
        to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas() {
        $result = [];

        $result['layout'] = $this->getGameStateValue('layout');

        $current_player_id = self::getCurrentPlayerId();
        $result['players'] = self::getCollectionFromDb(
            'select `player_id` as `id`, `player_score` as `score`
             from `player`'
        );
  
        // Gather all information about current game situation (visible
        // by player $current_player_id).
        $result['board'] = $this->board;
        $result['colors'] = $this->colors;
        $result['shapes'] = $this->shapes;
        $result['squares'] = self::getCollectionFromDb(
            'select `id`, `x`, `y` from `square`'
        );
        $result['pieces'] = self::getCollectionFromDb(
            'select `id`, `shape`, `is_white` from `piece`'
        );
        $result['moves'] = self::getObjectListFromDB(
            'select `move`.`id` as `id`, `player_id`, `piece_id`, `x`, `y`,
                    `shape`
             from `move`
             left join `square` on `square_id` = `square`.`id`
             left join `piece` on `piece_id` = `piece`.`id`'
        );

        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.  The number
        returned must be an integer beween 0 (=the game just started)
        and 100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the
        "updateGameProgression" property set to true (see
        states.inc.php)
    */
    function getGameProgression()
    {
        return round(
            self::getNonEmptyObjectFromDB(
                'select count(*) as `count` from `move`
                 where `piece_id` is not null'
            )['count'] / 16 * 100
        );
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Utility functions
////////////    

    function sql_placeholders (string $sql, string ...$args) {
        if ( substr_count($sql, '?') != count($args) ) {
            throw new BgaSystemVisibleException(
                'Insufficient arguments for pattern'
            );
        }

        foreach ( $args as $arg ) {
            $sql = preg_replace(
                '/\?/',
                "'". self::escapeStringForDB($arg). "'",
                $sql,
                1
            );
        }

        return $sql;
    }

    function get_legal_squares_for_latest_move () {
        $latest = self::getObjectFromDB(
            'select `x`, `y`, `shape`
             from `move`
             left join `square` on `move`.`square_id` = `square`.`id`
             left join `piece` on `move`.`piece_id` = `piece`.`id`
             order by `move`.`id` desc
             limit 1'
        );

        if ( $latest == null ) {
            if ( $this->getGameStateValue('layout') == 1 ) {
                return [
                    0 => [true, true,  true,  true],
                    1 => [true, false, false, true],
                    2 => [true, false, false, true],
                    3 => [true, true,  true,  true],
                ];
            }
            else {
                return self::get_edge_squares();
            }
        }
        else if ( $latest['shape'] == null ) {
            return self::get_legal_squares();
        }
        else {
            return self::get_legal_squares(
                $latest['x'], $latest['y'], $latest['shape']
            );
        }
    }

    function get_legal_squares ($x = null, $y = null, $shape = null) {
        $legal_squares = [];

        $squares = self::getCollectionFromDB(
            'select `square`.`id` as `id`, `x`, `y`, `piece_id`
             from `square` left join `move`
             on `square`.`id` = `move`.`square_id`'
        );
        if ( $this->getGameStateValue('rules') == 1 ) {
            $squares_xy = self::get_squares_xy($squares);
        }

        foreach ( $squares as $s ) {
            if ( $shape == null ) {
                $legal_squares[$s['x']][$s['y']] = true;
            }
            else {
                $legal_squares[$s['x']][$s['y']] = false;
            }
            if ( $shape == null or $s['piece_id'] != null ) {
                continue;
            }
            switch ( $shape ) {
                case 'straight':
                    if ( $s['x'] == $x or $s['y'] == $y ) {
                        if ( $this->getGameStateValue('rules') == 1 ) {
                            $tx = $s['x'];
                            do {
                                $ty = $s['y'];
                                do {
                                    if ( empty($squares_xy[$tx][$ty]) ) {
                                        break 3;
                                    }
                                    $ty += $s['y'] < $y ? 1 : -1;
                                } while ( $s['y'] < $y ? $ty < $y : $ty > $y );
                                $tx += $s['x'] < $x ? 1 : -1;
                            } while ( $s['x'] < $x ? $tx < $x : $tx > $x );
                        }
                        $legal_squares[$s['x']][$s['y']] = true;
                    }
                    break;
                case 'diagonal':
                    if (
                        $s['x'] - $x == $s['y'] - $y
                        or $s['x'] + $s['y'] == $x + $y
                    ) {
                        if ( $this->getGameStateValue('rules') == 1 ) {
                            $tx = $s['x'];
                            $ty = $s['y'];
                            do {
                                if ( empty($squares_xy[$tx][$ty]) ) {
                                    break 2;
                                }
                                $tx += $s['x'] < $x ? 1 : -1;
                                $ty += $s['y'] < $y ? 1 : -1;
                            } while ( $s['x'] < $x ? $tx < $x : $tx > $x );
                        }
                        $legal_squares[$s['x']][$s['y']] = true;
                    }
                    break;
                case 'pusher':
                    if ( abs($s['x'] - $x) > 1 or abs($s['y'] - $y) > 1 ) {
                        $legal_squares[$s['x']][$s['y']] = true;
                    }
                    break;
                case 'puller':
                    if ( abs($s['x'] - $x) <= 1 and abs($s['y'] - $y) <= 1 ) {
                        $legal_squares[$s['x']][$s['y']] = true;
                    }
                    break;
            }
        }

        return $legal_squares;
    }

    function get_edge_squares ($x = null, $y = null, $shape = null) {
        $squares = self::get_squares();
        $squares_xy = self::get_squares_xy($squares);
        $legal_squares_xy = [];

        foreach ( $squares as $s ) {
            foreach (
                [
                    [ 0,  1],
                    [ 1,  0],
                    [ 0, -1],
                    [-1,  0],
                ] as $map
            ) {
                if (
                    empty($squares_xy[$s['x'] + $map[0]][$s['y'] + $map[1]])
                ) {
                    $legal_squares_xy[$s['x']][$s['y']] = true;
                    continue 2;
                }
            }
            $legal_sqaures_xy[$s['x']][$s['y']] = false;
        }

        return $legal_squares_xy;
    }

    function get_legal_boards () {
        $squares = self::get_squares();
        $squares_xy = self::get_squares_xy($squares);
        $legal_squares_xy = [];

        $mappings = [
            [ 0,  1],
            [-1,  1],
            [-2,  0],
            [-2, -1],
            [-1, -2],
            [ 0, -2],
            [ 1, -1],
            [ 1,  0],
        ];
        if ( $this->getGameStateValue('rules') == 2 ) {
            array_push(
                $mappings,
                [ 1,  1],
                [-2,  1],
                [-2, -2],
                [ 1, -2]
            );
        }

        foreach ( $squares as $s ) {
            if ( isset($legal_squares_xy[$s['x']][$s['y']]) ) {
                continue;
            }
            foreach ( $mappings as $map ) {
                $board = [$s['x'] + $map[0], $s['y'] + $map[1]];
                if (
                    empty($squares_xy[$board[0]][$board[1]])
                    and empty($squares_xy[$board[0]][$board[1] + 1])
                    and empty($squares_xy[$board[0] + 1][$board[1]])
                    and empty($squares_xy[$board[0] + 1][$board[1] + 1])
                ) {
                    $legal_squares_xy[$board[0]][$board[1]]
                        = true;
                }
            }
        }

        return $legal_squares_xy;
    }

    function get_squares () {
        return self::getCollectionFromDB('select `id`, `x`, `y` from `square`');
    }

    function get_squares_xy ($squares) {
        $squares_xy = [];

        foreach ( $squares as $s ) {
            $squares_xy[$s['x']][$s['y']] = true;
        }

        return $squares_xy;
    }

    function insert_board ($x0, $y0) {
        $product = [];
        for ( $x = $x0; $x - $x0 < 2; $x++ ) {
            for ( $y = $y0; $y - $y0 < 2; $y++ ) {
                $product[] = "($x, $y)";
            }
        }
        self::DbQuery(
            'insert into `square` (`x`, `y`) values '. join($product, ',')
        );
    }

    function pass ($player_id) {
        self::DbQuery(
            self::sql_placeholders(
                'insert into `move` set `player_id` = ?',
                $player_id
            )
        );
        self::incStat(1, 'passes', $player_id);
        self::incStat(1, 'forced_passes', self::getPlayerBefore($player_id));
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Player actions
//////////// 

    /*
        Each time a player is doing some game action, one of the methods
        below is called.  (note: each method below must match an input
        method in mijnlieff.action.php)
    */

    function place_board ($x, $y) {
        self::checkAction('place_board');
        self::insert_board($x, $y);

        self::notifyAllPlayers(
            'placed_board',
            clienttranslate('${player_name} placed a board segment'),
            [
                'player_id' => self::getActivePlayerId(),
                'player_name' => self::getActivePlayerName(),
                'x' => $x,
                'y' => $y,
            ]
        );

        $this->gamestate->nextState('placed_board');
    }

    function place_piece ($x, $y, $piece_id) {
        // Check that this is player's turn and that it is a "possible
        // action" at this game state (see states.inc.php)
        self::checkAction('place_piece');

        $player_id = self::getActivePlayerId();
        $player_name = self::getActivePlayerName();

        // Check that this square is free
        $square = self::getObjectFromDb(
            'select `square`.`id` as `id`, `x`, `y`, `player_id`
             from `square` left join `move`
             on `square`.`id` = `move`.`square_id`
             where `x` = \''. self::escapeStringForDB($x). '\'
             and `y` = \''. self::escapeStringForDB($y). '\'
             and `player_id` is null'
        );
        if ( $square == null ) {
            throw new BgaUserException( self::_("There is already a piece in this square; you cannot play there") );
        }

        // Check that the placement is legal:
        if ( ! self::get_legal_squares_for_latest_move()[$x][$y] ) {
            throw new BgaUserException(self::_("This placement is impossible because of your opponent's previous move"));
        }

        // Check that this piece has not already been played:
        $piece = self::getNonEmptyObjectFromDB(
            self::sql_placeholders(
                'select `square_id`, `shape`
                 from `piece` left join `move`
                 on `piece`.`id` = `move`.`piece_id`
                 where `piece`.`id` = ?',
                $piece_id
            )
        );
        if ( $piece['square_id'] != null ) {
            throw new BgaUserException( self::_("This piece has already been played") );
        }

        // Store the placement in the database.
        self::DbQuery(
            self::sql_placeholders(
                'insert into `move`
                 set `player_id` = ?, `piece_id` = ?, `square_id` = ?',
                $player_id,
                $piece_id,
                $square['id']
            )
        );

        self::incStat(1, 'played_count', $player_id);

        // Notify all players
        self::notifyAllPlayers(
            'placed_piece',
            clienttranslate('${player_name} placed a ${shape_i18n}'),
            [
                'i18n' => ['shape_i18n'],
                'player_id' => $player_id,
                'player_name' => $player_name,
                'x' => $x,
                'y' => $y,
                'piece_id' => $piece_id,
                'shape' => $piece['shape'],
                'shape_i18n' => $piece['shape'],
            ]
        );


        // Assemble grid for easy access whilst checking for scoring:
        $squares = self::getCollectionFromDb(
            'select `square`.`id` as `id`, `x`, `y`, `player_id`
             from `move` inner join `square`
             on `move`.`square_id` = `square`.`id`'
        );
        foreach ( $squares as $id => $square ) {
            $grid[$square['x']][$square['y']] = $square['player_id'];
        }

        // Check for lines starting/ending with the current piece:
        $delta = -1;
        foreach (
            [
                function ($n) { return $n - 1; },
                function ($n) { return $n; },
                function ($n) { return $n + 1; }
            ] as $f_x
        ) {
            foreach (
                [
                    function ($n) { return $n - 1; },
                    function ($n) { return $n; },
                    function ($n) { return $n + 1; }
                ] as $f_y
            ) {
                if (
                    isset($grid[$f_x($x)][$f_y($y)])
                    and $grid[$f_x($x)][$f_y($y)] == $player_id
                    and isset($grid[$f_x($f_x($x))][$f_y($f_y($y))])
                    and $grid[$f_x($f_x($x))][$f_y($f_y($y))] == $player_id
                ) {
                    ++ $delta;
                }
            }
        }

        // Check for lines centred on the current piece:
        foreach (
            [
                [
                    [-1, -1],
                    [ 1,  1],
                ],
                [
                    [ 0, -1],
                    [ 0,  1],
                ],
                [
                    [ 1, -1],
                    [-1,  1],
                ],
                [
                    [ 1, 0],
                    [-1, 0],
                ],
            ] as $map
        ) {
            $no_match = false;
            foreach ( $map as $coord ) {
                if (
                    ! (
                        isset($grid[$x + $coord[0]][$y + $coord[1]])
                        and $grid[$x + $coord[0]][$y + $coord[1]] == $player_id
                    )
                ) {
                    $no_match = true;
                }
            }
            if ( $no_match == false ) {
                ++ $delta;
            }
        }

        // Record and notify of the new score:
        if ( $delta > 0 ) {
            self::DbQuery(
                'update `player`
                 set `player_score` = `player_score` + '. self::escapeStringForDB($delta). '
                 where `player_id` = '. self::escapeStringForDB($player_id)
            );
            self::notifyAllPlayers(
                'score',
                ( $delta == 1 ? clienttranslate(
                    '${player_name} completed a new line of three pieces'
                ) : clienttranslate(
                    '${player_name} completed ${delta} new lines of three pieces'
                ) ),
                [
                    'player_id' => $player_id,
                    'player_name' => $player_name,
                    'x' => $x,
                    'y' => $y,
                    'delta' => $delta
                ]
            );
        }


        // Go to next game state
        $this->gamestate->nextState('placed_piece');
    }

    
//////////////////////////////////////////////////////////////////////////////
//////////// Game state arguments
////////////

    /*
        Here, you can create methods defined as "game state arguments"
        (see "args" property in states.inc.php).  These methods function
        is to return some additional information that is specific to the
        current game state.
    */

    function player_setup_turn_arg () {
        return [
            'legal_boards' => self::get_legal_boards(),
        ];
    }

    function player_turn_arg () {
        return [
            'legal_squares' => self::get_legal_squares_for_latest_move(),
        ];
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Game state actions
////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */
    
    function check_layout_action () {
        if ( $this->getGameStateValue('layout') == 1 ) {
            $this->gamestate->nextState('standard');
        }
        else {
            $this->gamestate->nextState('players');
        }
    }

    function check_start_action () {
        if (
            self::getUniqueValueFromDB('select count(*) from `square`') < 16
        ) {
            $this->gamestate->nextState('not_started');
        }
        else {
            $this->gamestate->nextState('started');
        }
    }

    function check_end_action () {
        // The game ends if a player has no pieces left to place at the
        // start of their turn.

        if (
            self::getUniqueValueFromDB(
                self::sql_placeholders(
                    'select count(*) from `move`
                     where `player_id` = ? and `piece_id` is not null',
                    self::getActivePlayerId()
                )
            ) == 8
        ) {
            $this->gamestate->nextState('ended');
        }
        else {
            $this->gamestate->nextState('not_ended');
        }
    }

    function check_forced_pass_action () {
        $legal_square_p = false;
        foreach ( self::get_legal_squares_for_latest_move() as $column ) {
            self::dump('column', $column);
            foreach ( $column as $square ) {
                self::dump('square', $square);
                if ( $square ) {
                    $legal_square_p = true;
                    break 2;
                }
            }
        }

        if ( $legal_square_p ) {
            $this->gamestate->nextState('play');
        }
        else {
            self::pass($this->getActivePlayerId());
            self::notifyAllPlayers(
                'pass',
                clienttranslate('${player_name} could make no legal moves, so was forced to pass'),
                [
                    'player_name' => self::getActivePlayerName()
                ]
            );
            $this->gamestate->nextState('pass');
        }
    }

    function next_turn_action () {
        self::incStat(1, 'turns_count');
        self::giveExtraTime(self::activeNextPlayer());
        $this->gamestate->nextState();
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Zombie
////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who
        has quit the game (= "zombie" player).  You can do whatever you
        want in order to make sure the turn of this player ends
        appropriately (ex: pass).
        
        Important: your zombie code will be called when the player
        leaves the game. This action is triggered from the main site and
        propagated to the gameserver from a server, not from a browser.
        As a consequence, there is no current player associated to this
        action. In your zombieTurn function, you must _never_ use
        getCurrentPlayerId() or getCurrentPlayerName(), otherwise it
        will fail with a "Not logged" error message.
    */

    function zombieTurn ($state, $active_player) {
        if ( $state['type'] === 'activeplayer' ) {
            switch ( $state['name'] ) {
                case 'player_turn':

                    self::pass($active_player);
                    $this->gamestate->nextState('pass');
                    break;
            }

            return;
        }

        throw new feException(
            'Zombie mode not supported at this game state: '. $state['name']
        );
    }


///////////////////////////////////////////////////////////////////////////////////:
////////// DB upgrade
//////////

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been
        published on BGA.  Once your game is on BGA, this method is
        called everytime the system detects a game running with your old
        Database scheme.  In this case, if you change your Database
        scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run
        with your new version.
    */
    
    function upgradeTableDb ($from_version) {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345
        
        // Example:
//        if( $from_version <= 1404301345 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "ALTER TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB( $sql );
//        }
//        if( $from_version <= 1405061421 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "CREATE TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB( $sql );
//        }
//        // Please add your future database scheme changes here
//
//
    }
}
