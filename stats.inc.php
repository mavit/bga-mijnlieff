<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/*
    !! After modifying this file, you must use "Reload statistics
       configuration" in BGA Studio backoffice
       ("Control Panel" / "Manage Game" / "Your Game")

    If your game is already public on BGA, please read the following
    before any change:
    http://en.doc.boardgamearena.com/Post-release_phase#Changes_that_breaks_the_games_in_progress

    Once you defined your statistics here, you can start using "initStat", "setStat" and "incStat" method
    in your game logic, using statistics names defined below.

*/

$stats_type = [
    'table' => [
        'turns_count' => [
            'id' => 10,
            'name' => totranslate('Number of turns'),
            'type' => 'int',
        ],
    ],
    'player' => [
        'played_count' => [
            'id' => 10,
            'name' => totranslate('Number of pieces played'),
            'type' => 'int',
        ],
        'passes' => [
            'id' => 11,
            'name' => totranslate('Number of passes'),
            'type' => 'int',
        ],
        'forced_passes' => [
            'id' => 12,
            'name' => totranslate('Number of times a pass was forced'),
            'type' => 'int',
        ],
        'is_white' => [
            'id' => 13,
            'name' => totranslate('Took the first turn'),
            'type' => 'bool',
        ],
    ],
];
