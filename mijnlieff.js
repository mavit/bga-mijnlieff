// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/**
 *
 * Mijnlieff user interface script
 * 
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define([
    "dojo","dojo/_base/declare",
    "ebg/core/gamegui",
    "ebg/counter",
    'ebg/scrollmap',
    'ebg/stock'
],
function (dojo, declare) {
    return declare("bgagame.mijnlieff", ebg.core.gamegui, {
        constructor: function(){
            this.scrollmap = new ebg.scrollmap();
        },
        
        /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedatas" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */
        
        setup: function( gamedatas )
        {
            this.layout = gamedatas.layout;
            this.board = gamedatas.board;
            this.colors = gamedatas.colors;
            this.shapes = gamedatas.shapes;
            this.hand_stocks = [];

            if ( this.layout > 1 ) {
                this.scrollmap.create(
                    dojo.byId('map_container'),
                    dojo.byId('map_scrollable'),
                    dojo.byId('map_surface'),
                    dojo.byId('map_scrollable_oversurface')
                );
                this.scrollmap.scroll(
                    - this.board['SQUARE_SIZE'],
                    - this.board['SQUARE_SIZE']
                );
                this.scrollmap.setupOnScreenArrows(this.board['GRID_SIZE']);
                dojo.connect(
                    dojo.byId('enlarge_scrollmap'),
                    'onclick',
                    this,
                    'onenlarge_scrollmap'
                );
            }

            for( var player_id in gamedatas.players ) {
                // Insert hands into the DOM:
                dojo.place(
                    this.format_block(
                        'jstpl_hand',
                        {
                            'player_id': player_id,
                        }
                    ),
                    dojo.byId(
                        player_id == this.player_id
                            ? 'mijnlieff_hand_wrapper'
                            : 'player_board_' + player_id
                    ),
                );

                // Configure the players' hands:
                this.hand_stocks[player_id] = new ebg.stock();
                this.hand_stocks[player_id].create(
                    this,
                    $('hand_' + player_id),
                    this.board['SQUARE_SIZE'],
                    this.board['SQUARE_SIZE'],
                );
                this.hand_stocks[player_id].setSelectionMode(
                    player_id == this.player_id ? 1 : 0
                );
                this.hand_stocks[player_id].setSelectionAppearance('class');
                this.hand_stocks[player_id].onItemCreate = dojo.hitch(
                    this,
                    function (piece_node, type_id, piece_id) {
                        this.add_tooltip_for_shape(
                            piece_node,
                            this.shapes[type_id % 4]
                        );
                        if ( player_id == this.player_id ) {
                            piece_node.setAttribute('draggable', true);
                            piece_node.addEventListener(
                                'dragstart',
                                this.ondragstart_piece.bind(this),
                            );
                        }
                    }
                );
                dojo.connect(
                    this.hand_stocks[player_id],
                    'onChangeSelection',
                    this,
                    'onchangeselection_hand',
                );

                // Define possible content of the hand:
                var shape_count = Object.keys(this.shapes).length / 2;
                this.hand_stocks[player_id].image_items_per_row = shape_count;
                for ( var player = 0; player < 2; ++ player ) {
                    for ( var shape = 0; shape < shape_count; ++ shape ) {
                        this.hand_stocks[player_id].addItemType(
                            shape + shape_count * player,
                            shape,
                            g_gamethemeurl + 'img/pieces.png',
                            shape + shape_count * player,
                        );
                    }
                }

                // Populate the hand with its current content:
                var is_white = (
                    gamedatas.players[player_id]['color'] == this.colors.white
                );
                for ( var i in gamedatas.pieces ) {
                    const piece = gamedatas.pieces[i]
                    if ( piece['is_white'] == is_white ) {
                        this.hand_stocks[player_id].addToStockWithId(
                            this.shapes[piece['shape']] +
                                shape_count * (is_white ? 1 : 0),
                            piece['id'],
                        );
                        dojo.addClass(
                            this.hand_stocks[player_id].getItemDivId(
                                piece['id']
                            ),
                            piece['is_white'] == 1
                                ? 'mijnlieff_white'
                                : 'mijnlieff_black'
                        );
                    }
                    // dojo.addClass(
                    //     dojo.byId(this.hand_stocks[player_id].getItemDivId(piece['id'])),
                    // );
                }
            }


            // Initialise the board:
            const fxs = [];
            for ( var id in gamedatas.squares ) {
                fxs.push(this.display_square_fx(gamedatas.squares[id]));
            }

            const combination = dojo.fx.combine(fxs)
            dojo.connect(
                combination, 'onEnd', this, dojo.hitch(
                    this, function () {
                        // Replay earlier moves.  This doesn't work
                        // before the squares have had chance to animate
                        // into position.
                        for ( const move of gamedatas.moves ) {
                            if ( move.piece_id ) {
                                move.in_setup = true;
                                move.delay = 50 * move.id;
                                move.fast_forward = move !== gamedatas.moves[gamedatas.moves.length - 1];
                                this.notif_placed_piece({'args': move});
                            }
                        }
                    }
                )
            );
            combination.play();


            // Setup game notifications to handle (see //
            // "setupNotifications" method below)
            this.setupNotifications();
        },


        ///////////////////////////////////////////////////
        //// Game & client states
        
        // onEnteringState: this method is called each time we are
        //                  entering into a new game state.  You can use
        //                  this method to perform some user interface
        //                  changes at this moment.
        onEnteringState: function(stateName, args) {
            console.debug('Entering state: ' + stateName);

            switch (stateName) {
            case 'player_setup_turn':
                if ( this.player_id != this.getActivePlayerId() ) {
                    break;
                }

                const legal_boards = args.args.legal_boards;
                for (
                    const x of Object.keys(legal_boards).sort((a, b) => a - b)
                ) {
                    for (
                        const y
                        of Object.keys(legal_boards[x]).sort((a, b) => a - b)
                    ) {
                        const xy = {
                            'x': x,
                            'y': y,
                        };
                        dojo.place(
                            this.format_block(
                                'jstpl_board_option', xy
                            ), dojo.byId('mijnlieff_board_options')
                        );
                        const [x_px, y_px] = this.board_position(xy);
                        this.slideToObjectPos(
                            dojo.byId('board_' + x + '_' + y),
                            dojo.byId('mijnlieff_background'),
                            x_px,
                            y_px,
                            1,
                        ).play();
                    }
                }
                this.addEventToClass(
                    'mijnlieff_board_option',
                    'onclick',
                    'onclick_board',
                );

                break;

            case 'player_turn':
                dojo.query('.mijnlieff_touch_tip').forEach(dojo.destroy);

                const legal_squares = args.args.legal_squares;
                for ( const x in legal_squares ) {
                    for ( const y in legal_squares[x] ) {
                        const s = dojo.byId(['square', x, y].join('_'));
                        if ( legal_squares[x][y] ) {
                            this.legalise_square(s);
                        }
                        else {
                            dojo.removeClass(s, 'mijnlieff_legal');
                            dojo.addClass(s, 'mijnlieff_illegal');
                        }
                    }
                }
                break;

            case 'dummmy':
                break;
            }
        },

        // onLeavingState: this method is called each time we are
        //                 leaving a game state.  You can use this
        //                 method to perform some user interface changes
        //                 at this moment.
        onLeavingState: function(stateName) {
            console.debug('Leaving state: ' + stateName);
            
            switch (stateName) {
            case 'player_setup_turn':
                dojo.query(".mijnlieff_board_option").forEach(
                    dojo.destroy
                );

            case 'dummmy':
                break;
            }               
        }, 

        // onUpdateActionButtons: in this method you can manage "action
        //                        buttons" that are displayed in the
        //                        action status bar (ie: the HTML links
        //                        in the status bar).
        onUpdateActionButtons: function(stateName, args) {
            console.debug('onUpdateActionButtons: ' + stateName);

            if ( this.isCurrentPlayerActive() ) {
                switch (stateName) {
/*               
                 Example:
 
                 case 'myGameState':
                    
                    // Add 3 action buttons in the action status bar:
                    
                    this.addActionButton( 'button_1_id', _('Button 1 label'), 'onMyMethodToCall1' ); 
                    this.addActionButton( 'button_2_id', _('Button 2 label'), 'onMyMethodToCall2' ); 
                    this.addActionButton( 'button_3_id', _('Button 3 label'), 'onMyMethodToCall3' ); 
                    break;
*/
                }
            }
        },        


        ///////////////////////////////////////////////////
        //// Utility methods
        
        board_position: function (square) {
            return [
                this.board['X_ORIGIN'] + square.x * (this.board['SQUARE_SIZE'] + this.board['SQUARE_GAP']) - this.board['SQUARE_GAP']/2,
                this.board['Y_ORIGIN'] + square.y * (this.board['SQUARE_SIZE'] + this.board['SQUARE_GAP']) - this.board['SQUARE_GAP']/2,
            ];
        },

        square_position: function (square) {
            return [
                this.board['X_ORIGIN'] + square.x * (this.board['SQUARE_SIZE'] + this.board['SQUARE_GAP']),
                this.board['Y_ORIGIN'] + square.y * (this.board['SQUARE_SIZE'] + this.board['SQUARE_GAP'])
            ];
        },

        check_move_complete: function check_move_complete () {
            const hand_stock = this.hand_stocks[this.player_id];

            if (
                this.selected_square == null
                    || hand_stock.getSelectedItems().length == 0
            ) {
                return;
            }

            return this.submit_move(
                hand_stock.getSelectedItems()[0]['id'],
                ...this.selected_square,
            );
        },

        submit_move: function submit_move (piece_id, x, y) {
            // Check that this action is possible (see "possibleactions"
            // in states.inc.php)
            if ( ! this.checkAction('place_piece') ) {
                return;
            }

            this.ajaxcall(
                "/mijnlieff/mijnlieff/place_piece.html",
                {
                    'lock': true,
                    'x': x,
                    'y': y,
                    'piece_id': piece_id,
                },
                this,
                function (result) {},
                function (is_error) {},
            );

            this.deselect_all_squares();
        },

        display_square_fx: function display_square(square) {
            const [x_px, y_px] = this.square_position(square);
            const node = dojo.place(
                this.format_block(
                    'jstpl_square', {
                        'x': square.x,
                        'y': square.y,
                    }
                ),
                dojo.byId('mijnlieff_background')
            );
            node.addEventListener('click', this.onclick_square.bind(this));
            node.addEventListener(
                'dragenter',
                (event) => {
                    if ( this.is_droppable_square(node) ) {
                        event.preventDefault();
                        node.classList.add('mijnlieff_dragover');
                    }
                }
            );
            node.addEventListener(
                'dragover',
                (event) => {
                    if ( this.is_droppable_square(node) ) {
                        event.preventDefault();
                    }
                }
            );
            node.addEventListener(
                'dragleave',
                (event) => {
                    node.classList.remove('mijnlieff_dragover');
                }
            );
            node.addEventListener(
                'drop',
                (event) => {
                    event.preventDefault();
                    node.classList.remove('mijnlieff_dragover');
                    this.submit_move(
                        event.dataTransfer.getData('text/plain').match(
                            /^hand_\d+_item_(\d+)$/
                        )[1],
                        square.x,
                        square.y,
                    );
                }
            );

            dojo.style(
                node, {
                    'background-image': dojo.style(
                        // Re-use the default BGA wooden background:
                        dojo.query('html')[0], 'backgroundImage'
                    ),
                    'background-position':
                    100 * square.x + '% ' + (100 + 100 * square.y) + '%',
                }
            )

            return dojo.fx.chain([
                // placeOnObjectPos() was misaligned for some reason?
                this.slideToObjectPos(
                    node,
                    dojo.byId('mijnlieff_background'),
                    x_px,
                    y_px,
                    1,
                ),
                dojo.fadeIn({
                    'node': node,
                })
            ]);
        },

        select_square: function select_square (id) {
            this.selected_square = [id.split('_')[1], id.split('_')[2]];
            dojo.addClass(id, 'mijnlieff_selected');
        },

        deselect_all_squares: function deselect_all_squares () {
            this.selected_square = null;
            dojo.query('.mijnlieff_square.mijnlieff_selected').forEach(
                function(s) {
                    dojo.removeClass(s, 'mijnlieff_selected');
                }
            );
        },

        is_droppable_square: function is_droppable_square (square) {
            return this.player_id == this.getActivePlayerId()
                && square.classList.contains('mijnlieff_legal');
        },

        legalise_square: function legalise_square (square) {
            dojo.addClass(square, 'mijnlieff_legal');
            dojo.removeClass(square, 'mijnlieff_illegal');
        },

        illegalise_all_squares: function illegalise_all_squares () {
            dojo.query('.mijnlieff_square.mijnlieff_legal').forEach(
                function(s) {
                    dojo.removeClass(s, 'mijnlieff_legal');
                    dojo.addClass(s, 'mijnlieff_illegal');
                }
            );
        },

        add_tooltip_for_shape: function add_tooltip_for_shape (
            piece_node, shape
        ) {
            this.addTooltip(
                piece_node.id,
                {
                    'straight': _('“Straight”.  When played, the other player must place their next piece in the same row or column as this one.  If they cannot, they must pass.'),
                    'diagonal': _('“Diagonal”.  When played, the other player must place their next piece diagonally to this one.  If they cannot, they must pass.'),
                    'pusher': _('“Pusher”.  When played, the other player must place their next piece so that it is not adjacent to this one.  If they cannot, they must pass.'),
                    'puller': _('“Puller”.  When played, the other player must place their next piece so that it is adjacent to this one.  If they cannot, they must pass.'),
                }[shape],
                '',
            );
        },

        ///////////////////////////////////////////////////
        //// Player's action
        
        onclick_board: function (evt) {
            dojo.stopEvent(evt);
            const split = evt.currentTarget.id.split('_');

            this.ajaxcall(
                "/mijnlieff/mijnlieff/place_board.html",
                {
                    'lock': true,
                    'x': split[1],
                    'y': split[2],
                },
                this,
                function (result) {},
                function (is_error) {},
            );
        },

        onclick_square: function (evt) {
            dojo.stopEvent(evt);

            if ( dojo.hasClass(evt.currentTarget.id, 'mijnlieff_illegal') ) {
                return;
            }

            if ( dojo.hasClass(evt.currentTarget.id, 'mijnlieff_selected') ) {
                this.deselect_all_squares();
            }
            else {
                this.deselect_all_squares();
                this.select_square(evt.currentTarget.id);
                this.check_move_complete();
            }
        },

        onchangeselection_hand: function onchangeselection_hand (
            stock_dom_id, item_id
        ) {
            this.check_move_complete();
        },

        onenlarge_scrollmap: function onenlarge_scrollmap (evt) {
            evt.preventDefault();
            dojo.style(
                $('map_container'),
                'height',
                (toint(dojo.style($('map_container'), 'height')) + 300) + 'px',
            );
        },

        ondragstart_piece: function ondragstart_piece (event) {
            event.dataTransfer.setData(
                'text/plain',
                event.target.id,
            );
            event.dataTransfer.effectAllowed = 'move';
        },

        ///////////////////////////////////////////////////
        //// Reaction to cometD notifications

        /*
            setupNotifications:
            
            In this method, you associate each of your game
            notifications with your local method to handle it.
            
            Note: game notification names correspond to
                  "notifyAllPlayers" and "notifyPlayer" calls in your
                  mijnlieff.game.php file.
        
        */
        setupNotifications: function() {
            dojo.subscribe('placed_board', this, 'notif_placed_board');
            dojo.subscribe('placed_piece', this, 'notif_placed_piece');
            this.notifqueue.setSynchronous('placed_piece', 1000);
            this.notifqueue.setSynchronous('pass', 2000);
            dojo.subscribe('score', this, 'notif_score');
        },

        notif_placed_board: function notif_placed_board (notif) {
            for ( let x = notif.args.x; x - notif.args.x < 2; x++ ) {
                for ( let y = notif.args.y; y - notif.args.y < 2; y++ ) {
                    this.display_square_fx({
                        'x': x,
                        'y': y,
                    }).play();
                }
            }
        },

        notif_placed_piece: function notif_placed_piece (notif) {
            const subid = [notif.args.x, notif.args.y].join('_');
            const square = $('square_' + subid);
            const hand_stock = this.hand_stocks[notif.args.player_id];
            const piece_in_stock = dojo.byId(
                hand_stock.getItemDivId(notif.args.piece_id)
            );

            // The previous latest piece is no-longer so.
            dojo.query('.mijnlieff_piece.mijnlieff_latest').forEach(
                node => dojo.removeClass(node, 'mijnlieff_latest')
            );
            if ( ! notif.args.in_setup ) {
                this.illegalise_all_squares();
            }

            // Extract a piece from the player's hand to place on the board:
            const piece = dojo.clone(piece_in_stock);
            dojo.attr(piece, 'id', 'piece_' + notif.args.piece_id);
            dojo.removeClass(piece, 'stockitem stockitem_selected');
            dojo.setStyle(piece, 'opacity', null);
            dojo.addClass(piece, 'mijnlieff_piece');
            dojo.place(piece, square);

            // Place it on the matching piece in the stock:
            this.placeOnObject(piece, piece_in_stock);

            // Animate a slide from the player panel to the intersection
            dojo.style(piece, 'zIndex', 1);
            const slide = this.slideToObject(
                piece,
                square,
                500,
                notif.args.delay,
            );
            if ( ! notif.args.fast_forward ) {
                dojo.connect(
                    slide,
                    'onEnd',
                    function () {
                        dojo.addClass(piece, 'mijnlieff_latest');
                    }
                );
            }

            slide.play();

            // Add a tooltip:
            this.add_tooltip_for_shape(piece, notif.args.shape);

            // Remove the original item from the stock:
            this.hand_stocks[notif.args.player_id].removeFromStockById(
                notif.args.piece_id
            );
        },

        notif_score: function notif_score (notif) {
            this.scoreCtrl[notif.args.player_id].incValue(notif.args.delta);

            points_star = dojo.byId('icon_point_' + notif.args.player_id);
            const shooting_star = dojo.clone(points_star);
            dojo.attr(shooting_star, 'id', 'mijnlieff_shooting_star');
            dojo.style(shooting_star, 'zIndex', 1);
            dojo.place(shooting_star, dojo.byId('mijnlieff_game_area'));
            this.placeOnObject(
                shooting_star,
                dojo.byId(['square', notif.args.x, notif.args.y].join('_')),
            );
            const slide = this.slideToObject(shooting_star, points_star);
            dojo.connect(
                slide,
                'onEnd',
                this,
                dojo.hitch(
                    this,
                    function() {
                        dojo.destroy(shooting_star);
                    }
                ),
            );
            slide.play();
        },
   });
});
