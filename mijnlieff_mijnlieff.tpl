{OVERALL_GAME_HEADER}

<!-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
     Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk> -->

<!-- This file is part of BGA Mijnlieff.

     BGA Mijnlieff is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     BGA Mijnlieff is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>. -->

<!-- This code has been produced on the BGA Studio platform for use on
     https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
     for more information. -->

<script type="text/javascript">
    // Javascript HTML templates
    var jstpl_board_option = '<div class="mijnlieff_board_option" id="board_${x}_${y}"></div>';
    var jstpl_square='<div class="mijnlieff_square" id="square_${x}_${y}"></div>';
    var jstpl_hand='<div class="mijnlieff_hand" id="hand_${player_id}"></div>';
</script>  

<div id="mijnlieff_game_area">
  <!-- BEGIN standard_layout -->
  <div id="mijnlieff_background" class="mijnlieff_layout_standard">
    <div id="mijnlieff_board"></div>
  </div>
  <div id="mijnlieff_hand_wrapper"></div>
  <!-- END standard_layout -->
  
  <!-- BEGIN scrollmap_layout -->
  <div id="map_container">
    <div id="map_scrollable"></div>
    <div id="map_surface"></div>
    <div id="map_scrollable_oversurface">
      <div id="mijnlieff_background" class="mijnlieff_layout_player"></div>
      <div id="mijnlieff_board_options"></div>
    </div>
    <a id="movetop" href="#"></a>
    <a id="moveleft" href="#"></a>
    <a id="moveright" href="#"></a>
    <a id="movedown" href="#"></a>
  </div>
  <div id="map_footer" class="whiteblock">
    <a href="#" id="enlarge_scrollmap">↓  {LABEL_ENLARGE_SCROLLMAP}  ↓</a>
  </div>
  <div class="mijnlieff_touch_tip">
    {LABEL_TOUCH_TIP}
  </div>
  <div id="mijnlieff_hand_wrapper"></div>
  <!-- END scrollmap_layout -->
</div>

{OVERALL_GAME_FOOTER}
