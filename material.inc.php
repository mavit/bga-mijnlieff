<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/**
 *
 * Mijnlieff game material description
 *
 * Here, you can describe the material of your game with PHP variables.
 *   
 * This file is loaded in your game logic class constructor, ie these variables
 * are available everywhere in your game logic code.
 *
 */

$this->board = [
    "X_ORIGIN" => 8,
    "Y_ORIGIN" => 8,
    'GRID_SIZE' => (320)/4,
    'SQUARE_SIZE' => (320 - 5*8)/4,
    'SQUARE_GAP' => 8,
    'PIECES_PER_SHAPE' => 2,
];

$this->colors = [
    'black' => '6f4324',
    'white' => 'f36220',
    '6f4324' => 'black',
    'f36220' => 'white'
];

$this->shapes = [
    0 => clienttranslate('straight'),
    1 => clienttranslate('diagonal'),
    2 => clienttranslate('pusher'),
    3 => clienttranslate('puller'),
    'straight' => 0,
    'diagonal' => 1,
    'pusher' => 2,
    'puller' => 3,
];
