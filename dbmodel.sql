-- -*- mode: sql; sql-product: mysql -*-

-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

-- This file is part of BGA Mijnlieff.
--
-- BGA Mijnlieff is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- BGA Mijnlieff is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

-- This code has been produced on the BGA Studio platform for use on
-- https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
-- for more information.


create table if not exists `square` (
   `id` tinyint(2) unsigned not null auto_increment,
   `x` tinyint(1) signed not null,
   `y` tinyint(1) signed not null,
   primary key (`id`),
   unique index (`x`, `y`)
) engine=InnoDB default charset=utf8 auto_increment=1;

create table if not exists `piece` (
   `id` tinyint(2) unsigned not null auto_increment,
   `shape` varchar(8) not null,
   `is_white` bool not null,
   primary key (`id`)
) engine=InnoDB default charset=utf8 auto_increment=1;

create table if not exists `move` (
   `id` tinyint(2) unsigned not null auto_increment,
   `player_id` int(10) unsigned null,
   `square_id` tinyint(2) unsigned null unique,
   `piece_id` tinyint(2) unsigned null unique,
   primary key (`id`),
   foreign key (`square_id`) references `square`(`id`),
   foreign key (`piece_id`) references `piece`(`id`)
) engine=InnoDB default charset=utf8 auto_increment=1;
