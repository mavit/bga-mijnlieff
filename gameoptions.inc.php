<?php

// BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
// Mijnlieff implementation: Copyright 2020 Peter Oliver <bga@mavit.org.uk>

// This file is part of BGA Mijnlieff.
//
// BGA Mijnlieff is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BGA Mijnlieff is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with BGA Mijnlieff.  If not, see <https://www.gnu.org/licenses/>.

// This code has been produced on the BGA Studio platform for use on
// https://boardgamearena.com.  See https://boardgamearena.com/doc/Studio
// for more information.

/**
 *
 * Mijnlieff game options description
 *
 * In this file, you can define your game options (= game variants).
 *   
 * Note: If your game has no variant, you don't have to modify this file.
 *
 * Note²: All options defined in this file should have a corresponding "game state labels"
 *        with the same ID (see "initGameStateLabels" in mijnlieff.game.php)
 *
 * !! It is not a good idea to modify this file when a game is running !!
 *
 */

$game_options = [
    // note: game variant ID should start at 100 (ie: 100, 101, 102, ...). The maximum is 199.

    100 => [
        'name' => totranslate('Board layout'),
        'values' => [
            1 => [
                'name' => totranslate('Standard'),
            ],
            2 => [
                'name' => totranslate('Arranged by players'),
                'tmdisplay' => totranslate('Board arranged by players'),
                'description' => totranslate('The players will take it in turns to lay 4×4 board segments'),
                'nobeginner' => true,
            ],
        ],
        'default' => 1,
    ],
    101 => [
        'name' => totranslate('Board layout rules'),
        'values' => [
            1 => [
                'name' => totranslate('Walled'),
                'tmdisplay' => '',
                'description' => totranslate('Gaps between squares of the board block straights and diagonals.  Board segments must be placed such that a square is orthogonally adjacent to an existing square.'),
            ],
            2 => [
                'name' => totranslate('Unofficial variant: Open'),
                'tmdisplay' => totranslate('Open rules variant'),
                'description' => totranslate('Straights and diagonals permit placement across gaps in the board.  Board segments may be diagonally adjacent.'),
            ],
        ],
        'default' => 1,
        'displaycondition' => [
            [
                'type' => 'otheroption',
                'id' => 100,
                'value' => 2,
            ]
        ]
    ],
];
